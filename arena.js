/*import { Warrior } from "./index.js";
let beta = new Warrior("beta", 100, 100);
let nala = new Warrior("nala", 50, 1000);

console.log(beta.life);
nala.attack(beta);
console.log(beta.life);
nala.attack(beta);
console.log(beta.life);
console.log(beta.isAlive());
*/

import { WarriorAxe } from "./index.js";
import { WarriorSword } from "./index.js";
import { WarriorSpear } from "./index.js";
let lance = new WarriorSpear("lance", 10, 20);
let epee = new WarriorSword("epee", 10, 20);
let hache = new WarriorAxe("hache", 10, 20);
/*
console.log(lance.life);
epee.attack(lance);
console.log(lance.life);
console.log(hache.life);
epee.attack(hache);
console.log(hache.life);
*/

do {
  lance.attack(epee);
  epee.attack(lance);

  //console.log(lance.life + " lance | epee " + epee.life);
} while (epee.life > 0 && lance.life > 0);

if (epee.life <= 0 && lance.live <= 0) {
  console.log("It's a draw");
} else if (epee.life > 0) {
  console.log(epee.name + " wins");
} else {
  console.log(lance.name + " wins");
}
