export class Warrior {
  constructor(name, power, life) {
    this.name = name;
    this.power = power;
    this.life = life;
  }

  attack(opponent) {
    if (opponent instanceof Warrior) {
      opponent.life -= this.power;
    }
  }

  isAlive() {
    return this.life > 0;
  }
}

export class WarriorAxe extends Warrior {
  attack(opponent) {
    if (opponent instanceof WarriorSword) {
      opponent.life -= this.power * 2;
    } else {
      opponent.life -= this.power;
    }
  }
}
export class WarriorSword extends Warrior {
  attack(opponent) {
    if (opponent instanceof WarriorSpear) {
      opponent.life -= this.power * 2;
    } else {
      opponent.life -= this.power;
    }
  }
}
export class WarriorSpear extends Warrior {
  attack(opponent) {
    if (opponent instanceof WarriorAxe) {
      opponent.life -= this.power * 2;
    } else {
      opponent.life -= this.power;
    }
  }
}
